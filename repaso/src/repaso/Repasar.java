package repaso;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JTree;

public class Repasar extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Repasar frame = new Repasar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Repasar() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 128, 64));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Buenas tardes");
		lblNewLabel.setBounds(48, 38, 94, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("mariconaso");
		btnNewButton.setForeground(new Color(255, 0, 128));
		btnNewButton.setBackground(new Color(255, 0, 128));
		btnNewButton.setBounds(48, 75, 89, 23);
		contentPane.add(btnNewButton);
		
		JTree tree = new JTree();
		tree.setBounds(175, 37, 72, 71);
		contentPane.add(tree);
	}
}
